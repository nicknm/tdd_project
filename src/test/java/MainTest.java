import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class MainTest {

    public static final String annaJwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbm5hIiwiUk9MRSI6IkFETUlOIn0.xXl4cNFQAO1kqXLhHQfA6tCwFmnKq007hX70ck7l3XA";
    public static final String beritJwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJiZXJpdCIsIlJPTEUiOiJURUFDSEVSIn0.kAjaI6j8-Ny8-N0cjgYoIHk5O5MEqhmCUfBqcwriipI";
    public static final String kalleJwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJrYWxsZSIsIlJPTEUiOiJTVFVERU5UIn0.kbvfBNAkFGb-0xRrf65Kr6Yj6YMk-gQJkEMBIORdC4Q";

    @ParameterizedTest
    @CsvSource(value = {"anna, losen, YW5uYQ==", "berit, 123456, YmVyaXQ=", "kalle,password, a2FsbGU="})
    public void test_login_success(String username, String password, String realToken) throws UnsupportedEncodingException {
        //When
        Main main = new Main();
        String nameInBase64 = main.login(username, password);

        //Then
        assertEquals(realToken, nameInBase64);
    }

    @ParameterizedTest
    @CsvSource(value = {"anna, ", "berit, 12456", "kalle,losen"})
    public void test_login_failed(String username, String password){
        //When
        Main main = new Main();
        UnsupportedEncodingException ex =
                assertThrows(UnsupportedEncodingException.class, () ->
                        main.login(username, password));

        //Then
        assertEquals("You entered the wrong username or password", ex.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {"YW5uYQ==", "YmVyaXQ=", "a2FsbGU="})
    public void test_token_validation_success(String token) {
        //When
        Main main = new Main();
        boolean tokenPassed = main.verifyToken(token);

        //Then
        assertTrue(tokenPassed);
    }

    @ParameterizedTest
    @ValueSource(strings = {"YW5uYl==", "YmVyaXl=", "a2FsbGl="})
    public void test_token_validation_fail(String token){
        //When
        Main main = new Main();
        boolean tokenPassed = main.verifyToken(token);

        //Then
        assertFalse(tokenPassed);
    }

    @ParameterizedTest
    @MethodSource("usernamePasswordToken")
    public void test_jwtToken_generation_success(String username, String inputPassword, String expectedJwt){
        //When
        Main main = new Main();
        String Jwt = main.buildJwt(username, inputPassword);

        //Then
        assertEquals(expectedJwt, Jwt);
    }

    @ParameterizedTest
    @MethodSource("jwtDomainAndExpectedPermissions")
    public void test_jwtToken_validation_success(String jwtToken, String domain, String expectedAccess) throws UnsupportedEncodingException {
        //When
        ArrayList<String> expectedAccessList = new ArrayList<>();
        Main main = new Main();
        ArrayList access = main.verifyJwt(jwtToken, domain);

        System.out.println(access);

        expectedAccessList.add(domain + ": " + expectedAccess);

        //Then
        assertEquals(expectedAccessList, access);
    }

    @ParameterizedTest
    @ValueSource(strings = {"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbm5hIiwiUk9MRSI6IkFETUlOIn0.xXl3cNFQAO1iqXLhHQfA6tCwFmnKq007hX70ck7l3XB"})
    public void test_jwtToken_validation_failed(String jwtToken){
        //When
        Main main = new Main();
        UnsupportedEncodingException ex =
                assertThrows(UnsupportedEncodingException.class, () ->
                        main.verifyJwt(jwtToken, "GRADING"));


        //Then
        assertEquals("The JWT token is not correct", ex.getMessage());
    }

    private static Stream<Arguments> jwtDomainAndExpectedPermissions() {
        return Stream.of(
                arguments(annaJwt, "Course Feedback", "READ/WRITE"),
                arguments(beritJwt, "GRADING", "READ/WRITE"),
                arguments(kalleJwt, "GRADING", "READ")
        );
    }

    private static Stream<Arguments> usernamePasswordToken() {
        return Stream.of(
                arguments("anna", "losen", annaJwt),
                arguments("berit", "123456" , beritJwt),
                arguments("kalle", "password", kalleJwt)
        );
    }
}
