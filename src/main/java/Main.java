import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class Main {

    public static Map getUsers(){
        Map<String, String> users = new HashMap<>();
        users.put("anna", "losen");
        users.put("berit", "123456");
        users.put("kalle", "password");
        return users;
    }

    public String login(String username, String inputPassword) throws UnsupportedEncodingException {
        Map<String, String> users = getUsers();
            if(Objects.equals(users.get(username), inputPassword)){
                byte[] nameAsBytes = username.getBytes();
                String nameInBase64 = Base64.getEncoder().encodeToString(nameAsBytes);
                return nameInBase64;
            } else {
                throw new UnsupportedEncodingException("You entered the wrong username or password");
            }
    }

    public boolean verifyToken(String token) {
            byte[] decodedValue = Base64.getDecoder().decode(token);
            String nameInString = new String(decodedValue);
            if(getUsers().containsKey(nameInString)){
                return true;
            }
            return false;
    }

    String nyckel = "this-is-a-very-secret-key";
    byte[] secretBytes = Base64.getEncoder().encode(nyckel.getBytes());
    SecretKey secretKey = new SecretKeySpec(secretBytes, "HmacSHA256");

    public String buildJwt(String username, String inputPassword) {
        Map<String, String> users = getUsers();
        if (Objects.equals(users.get(username), inputPassword)) {
            String role = "";

            switch (username) {
                case "anna":
                    role = "ADMIN";
                    break;
                case "berit":
                    role = "TEACHER";
                    break;
                case "kalle":
                    role = "STUDENT";
                    break;
            }

            String jws = Jwts.builder()
                    .setSubject(username)
                    //.setExpiration(new Date(System.currentTimeMillis() + 10000*60))
                    .claim("ROLE", role)
                    .signWith(secretKey)
                    .compact();
            return jws;
        }
        return "Something went horribly wrong.";
    }

    //Ta in domänen också
    //Returnera en lista med rättigheter för endast den domänen
    public ArrayList<String> verifyJwt(String jwtToken, String domain) throws UnsupportedEncodingException {

        ArrayList<String> gradingAccessList = new ArrayList<>();
        ArrayList<String> feedbackAccessList = new ArrayList<>();

        String role = "";

        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(secretKey)
                    .build()
                    .parseClaimsJws(jwtToken)
                    .getBody();

            role = claims.get("ROLE").toString();
            System.out.println(role);
        } catch (Exception ex) {
            throw new UnsupportedEncodingException("The JWT token is not correct");
        }

        switch (role) {
            case "ADMIN":
                gradingAccessList.add("GRADING: READ/WRITE");
                feedbackAccessList.add("Course Feedback: READ/WRITE");
                break;
            case "TEACHER":
                gradingAccessList.add("GRADING: READ/WRITE");
                feedbackAccessList.add("Course Feedback: READ");;
                break;
            case "STUDENT":
                gradingAccessList.add("GRADING: READ");
                feedbackAccessList.add("Course Feedback: READ/WRITE");
                break;
        }

       if (domain.equals("GRADING")) {
            return gradingAccessList;
        } else if (domain.equals("Course Feedback")) {
            return feedbackAccessList;
        }

        return gradingAccessList;
    }
}
